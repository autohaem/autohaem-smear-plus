include <./smear_plus_parameters.scad>;
use <../autohaem-smear/openscad/utilities.scad>;
use <../autohaem-smear/openscad/shared_main_body_objects.scad>;
use <../autohaem-smear/openscad/hinge_component.scad>;

module each_nema17_screw(){
    repeat([0,31,0],2)repeat([0,0,31],2) children();
}
nema_support_depth = 6;

module base(){
    translate([wall_thickness, wall_thickness,0])difference(){
        minkowski(){
            cube(base_size);
            cylinder(r=wall_thickness,h = tiny(),$fn=100);
        }
        translate([0,0,wall_thickness])cube(base_size);
    }
}

module base_holes(){
    //nema 17 screw holes
    translate([100-2.5-vibration_dampener_depth,-nema_17_size.y+5.65-1,rod_height+box_size.z-nema_17_size.z/2+5.65])each_nema17_screw(){
        rotate([0,0,90])screw_y(d=3,h=4, shaft=true, extra_height= 0.2,$fn=100);
    }
    //nema motor main hole
    translate([20, -nema_17_size.y/2-1, rod_height+box_size.z])rotate([0,0,-90])cylinder_with_45deg_top(r=25/2,h=100,$fn=100);
}

module side_hole(){
    rotate([90,0,0]){
        minkowski(){
            cube([13,13,20]);
            cylinder(r=radius_of_curvature,h=tiny(),$fn=50);
        }
    }
}

module side_holes(){
    translate([arduino_position.x-4,base_size.y+10,9]){
        rotate([90,0,0]){
            minkowski(){
                cube([10,11,20]);
                cylinder(r=radius_of_curvature,h=tiny(),$fn=50);
            }
        }
    }
    translate([arduino_position.x+31-13/2,base_size.y+10,9]){
        rotate([90,0,0]){
            minkowski(){
                cube([13,12,20]);
                cylinder(r=radius_of_curvature,h=tiny(),$fn=50);
            }
        }
    }
    translate([arduino_position.x+60,base_size.y,9+6]){
            cylinder_with_45deg_top(r=4,h=10,$fn=50,extra_height=0.4);
    }
    translate([base_size.x,11+radius_of_curvature,base_size.z-2]){
        rotate([90,0,90]){
            minkowski(){
                cube([13,13,20]);
                cylinder(r=radius_of_curvature,h=tiny(),$fn=50);
            }
        }
    }
}

module lid_mounts(){
    intersection(){
            translate([base_size.x/2+wall_thickness,base_size.y/2+wall_thickness,0])reflect([0,1,0])reflect([1,0,0]){
            translate([base_size.x/2,base_size.y/2,0]){
                hull(){
                    rotate([0,0,45])translate([0,0,base_size.z-5])cylinder(r=8,h=5,$fn=3);
                    translate([0,0,base_size.z/3])cylinder(r=tiny(),h=tiny(),$fn=100);
                }
            }
        }
         hull(){
            each_lid_mount(){
                cylinder(r=3.5,h=base_size.z,$fn=100);
            }
        }
    }
}

module lid_mount_holes(){
    translate([base_size.x/2+wall_thickness,base_size.y/2+wall_thickness,base_size.z-5])reflect([0,1,0])reflect([1,0,0]){
        translate([-(base_size.x-1)/2,-(base_size.y-1)/2,0]){
            intersection(){
                rotate(-45)nut_from_bottom(d = 3, h = 3,chamfer_r = 0, chamfer_h=0);
                translate([0,0,5])cube(10,center=true);
            }
            rotate([0,0,45])hull(){
                rotate(30)nut(d = 3, h = 3);
                translate([10,0,0])rotate(30)nut(d = 3, h = 3);
            }
        }
    }
}

module each_lid_mount(){
    translate([wall_thickness+0.5,wall_thickness+0.5,0])repeat([base_size.x-1,0,0],2)repeat([0,base_size.y-1,0],2)children();
}


module nema_motor(){
    translate([100-vibration_dampener_depth,-nema_17_size.y-2-1,rod_height+box_size.z-nema_17_size.z/2-2])cube(nema_17_size+[vibration_dampener_depth,2+1,2]);
}

module nema_motor_support(){
    difference(){
        hull(){
            translate([100-nema_support_depth+radius_of_curvature-vibration_dampener_depth,-(nema_17_size.y+6)+radius_of_curvature,0]){
                minkowski(){
                    cube([nema_17_size.x+vibration_dampener_depth+nema_support_depth-2*radius_of_curvature,nema_17_size.y+6-2*radius_of_curvature,rod_height+box_size.z-nema_17_size.z/2]);
                    cylinder(r=radius_of_curvature,h=tiny(),$fn=50);
                }
            }
            translate([100-nema_support_depth+radius_of_curvature-vibration_dampener_depth,-(nema_17_size.y+6)+radius_of_curvature,rod_height+box_size.z-nema_17_size.z/2]){
                minkowski() {
                    cube([nema_support_depth-2*radius_of_curvature,nema_17_size.y+6-2*radius_of_curvature,nema_17_size.z]);
                    cylinder(r=radius_of_curvature,h=tiny(),$fn=50);
                }
            }
            
        }
    nema_motor();
    }
}

module each_arduino_support_right(){
    repeat([27.9,0,0],2) children();
}

module each_arduino_support_left(){
    repeat([47.8,-1.1,0],2) children();
}


module arduino_supports(){
    translate([-(7.6-2.5),51.9,0])each_arduino_support_left(){
        cylinder(r = 3.5, h=6,$fn=50);
        cylinder(r = 5, h=4.5,$fn=50);
    }
    translate([0,0,0])each_arduino_support_right(){
        cylinder(r = 3.5, h=6,$fn=50);
        cylinder(r = 5, h=4.5,$fn=50);
    }
}

module arduino_supports_holes(){
    translate([-(7.6-2.5),51.9,1])each_arduino_support_left(){
        cylinder(d = 3, h=5,$fn=50);
        translate([0,0,-1-tiny()])nut_from_bottom(d=3,h=4);
    }
    translate([0,0,1])each_arduino_support_right(){
        cylinder(d = 3, h=5,$fn=50);
        translate([0,0,-1-tiny()])nut_from_bottom(d=3,h=4);
    }
}


module each_stepper_support(){
    repeat([35.6,0,0],2) repeat([0,35.6,0],2) children();
}

module stepper_supports(){
    each_stepper_support(){
        cylinder(r = 3.5, h=6,$fn=50);
        cylinder(r = 5, h=4.5,$fn=50);
    }
}

module stepper_supports_holes(){
    translate([0,0,1])each_stepper_support(){
        cylinder(d = 3, h=5,$fn=50);
        translate([0,0,-1-tiny()])nut_from_bottom(d=3,h=4);
    }
}

module complete_base(){
    difference(){
        union(){
            difference(){
                union(){
                    base();
                    translate([0,tiny(),0])nema_motor_support();
                    //nema_motor();
                    //translate(stepper_position-[3.5,3.5,0])cube(stepper_dimensions);
                    //translate(arduino_position-[7.6,3.6,0])cube(arduino_dimensions);

                }
                base_holes();
                side_holes();
                translate([0,base_size.y/2,-3])logo(version_numstring);
            }
            translate(arduino_position)arduino_supports();
            translate(stepper_position)stepper_supports();
            lid_mounts();
        }
        translate(arduino_position)arduino_supports_holes();
        translate(stepper_position)stepper_supports_holes();
        lid_mount_holes();
    }
}

complete_base();