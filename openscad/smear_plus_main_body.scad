use <../autohaem-smear/openscad/shared_main_body_objects.scad>;
use <../autohaem-smear/openscad/utilities.scad>;
use <./limit_switch_lid.scad>;
use <./base.scad>;
include <./smear_plus_parameters.scad>;

module button_hole(){
    intersection(){
        cylinder(r=5, h=999, center=true); 
        translate([0,0,lid_thickness-2]) hole_from_bottom(r=4.1, h=999, base_w=999, big_bottom=true);
    }
}

module rod_holders(){
    difference(){
        repeat([limit_switch_offset.x+limit_switch_size.x/2-rod_holder_size.x/2+limit_switch_wall_thickness,0,0],2){
            difference(){
                translate([radius_of_curvature,radius_of_curvature,0]){
                    minkowski(){
                        cube(rod_holder_size-[2*radius_of_curvature,2*radius_of_curvature,0]);
                        cylinder(r=radius_of_curvature, h=tiny(),$fn=100);
                    }
                }
                translate([rod_holder_size.x/2,rod_holder_size.y/2,rod_height*2-3]){intersection(){
                    rotate(30)nut_from_bottom(d = 3, h = 3,chamfer_r = 0, chamfer_h=0);
                    translate([0,0,5])cube(10,center=true);
                }
                hull(){
                    rotate(30)nut(d = 3, h = 3);
                    translate([0,-10,0])rotate(30)nut(d = 4, h = 3);
                }
                translate([0,0,-2])cube([4,4,4],center=true);
                }
                //translate([rod_holder_size.x/2,rod_holder_size.y/2,hinge_box_size.z/2])cylinder(d=3,h=rod_holder_size.z,$fn=50);
                //hull(){
                //    translate([rod_holder_size.x/2,rod_holder_size.y/2,rod_holder_size.z-2-3])nut(d = 3.2, h = 3);
                //    translate([rod_holder_size.x/2,0,rod_holder_size.z-2-3])nut(d = 3.2, h = 3);
//
                //}
            }
        }
        translate([-tiny(), rod_holder_size.y/2, rod_height+rod_radius_slide_bodge/2]){
            rotate([0,0,-90]){cylinder_with_45deg_top(h = rod_length,extra_height=0.2, r = rod_radius+rod_radius_slide_bodge,$fn = 100);}
        }
    }

}


module smear_plus_main_box(){
    translate([wall_thickness, wall_thickness,0])
    minkowski(){
        cube([base_size.x,base_size.y,lid_thickness]);
        cylinder(r=wall_thickness,h = tiny(),$fn=100);
    }
}

module main_body(){
    difference(){
        union(){
            smear_plus_main_box(); //main box
            translate([0,box_size.y+10-rod_holder_size.y,lid_thickness])rod_holders();
            limit_switch_box();
        }
        translate([microscope_slide_offset, box_size.y/2-(microscope_slide_size.y+microscope_slide_bodge_factor)/2,(lid_thickness-microscope_slide_size.z)]){
            microscope_slide_and_dips();//Space for microscope slide
        }
        each_lid_mount(){
            translate([0,0,lid_thickness-2.9])cylinder(d=5.7,h=3,$fn=100);
            cylinder(d=3,h=100,$fn=100);
        }
        // Button space
        translate([limit_switch_offset.x+limit_switch_size.x/2+limit_switch_wall_thickness,base_size.y-6,0])button_hole();
        // limit switch wire hole
        translate(limit_switch_offset+[(limit_switch_size+limit_switch_bodge_factor).x-radius_of_curvature,(limit_switch_size+limit_switch_bodge_factor).y/2+limit_switch_wall_thickness,0]){cube([4,limit_switch_size.y,100], center=true);
        }
        translate(limit_switch_offset+[0,1,lid_thickness]){
            limit_switch_2mm_holes();
        }
    }
}

main_body();




