# 8mm Diameter LED PushButton

RS PRO Single Pole Single Throw (SPST) Momentary Red LED Miniature Push Button Switch, 8 (Dia.)mm, PCB, 30V dc,

* 8mm diameter circular base
* 23mm tall

Available from e.g.  [RS Stock no. 820-7521](https://uk.rs-online.com/web/p/push-button-switches/8207521/)