# Assemble Base

{{BOM}}

[Arduino Uno]: components/Arduino_uno.md "{cat:part}"
[base]: models/base.stl "{cat:3DPrinted}"
[main body]: models/smear_plus_main_body.stl "{cat:3Dprinted}"
[M3x6mm screw]: "{cat:part}"
[M3 nut]: "{cat:part}"
[button]: components/LED_button.md "{cat:part}"
[Nema 17 motor with lead screw]: components/nema_motor.md#motor "{cat:part}"
[Nema 17 vibration dampener]: components/nema_motor_vibration_dampener.md "{cat:part}"
[Drv8825 Stepper Driver]: components/Drv8825.md "{cat:part}"
[42CH Stepper Motor Driver Expansion Board]: components/expansion_board.md "{cat:part}"
[motor connection cable]: components/nema_motor.md#cable "{cat:part}"
[M2.5 hex screwdriver]: "{cat:tool}"
[DC socket nut]: components/DC_socket.md#nut "{cat:part}"
[DC socket washer]: components/DC_socket.md#washer "{cat:part}"

# Method

## Attach Arduino Uno onto the Base {pagestep}

Push three [M3 nut]{qty:3}s into the holes underneath the base. Place the [Arduino Uno]{qty:1} onto the raised standoffs on the [base]{qty:1}.  Secure it in place using three [M3x6mm screw]{qty:3}s with a [M2.5 hex screwdriver].

![](images/assemble_base/base_arduino_nuts.jpg)
![](images/assemble_base/mounting_arduino.jpg)

## Attach Stepper Driver onto the the Base {pagestep}

Attach the [Drv8825 Stepper Driver]{qty:1} onto the [42CH Stepper Motor Driver Expansion Board]{qty:1} by inserting the 16 pins of the driver into the red ports of the extension board. Make sure the VIN pin is in the correct hole.

Push four [M3 nut]{qty:4}s into the holes underneath the base. Attach the driver-expansion board onto the raised standoffs on the [base]. Secure it in place using four [M3x6mm screw]{qty:4}s with a [M2.5 hex screwdriver].

![](images/assemble_base/expansion_board.jpg)
![](images/assemble_base/driver_and_expansion_board.jpg)
![](images/assemble_base/base_stepper_nuts.jpg)
![](images/assemble_base/mounting_driver.jpg)

## Attach the Nema Motor {pagestep}

Attach the [Nema 17 vibration dampener]{qty:1} to the [Nema 17 motor with lead screw]{qty:2} using two [M3x6mm screw]{qty:2}s with a [M2.5 hex screwdriver]{qty:1}.

![](images/assemble_base/vibration_dampener_mount.jpg)

Place the [Nema 17 vibration dampener] on the [base] and secure using two [M3x6mm screw]{qty:2}s with a [M2.5 hex screwdriver]{qty:1}, with its socket facing towards the main body.

![](images/assemble_base/mount_motor.jpg)


## Make the connections {pagestep}

Connect the [motor connection cable]{qty:1} from the [Nema 17 motor with lead screw] to the socket of the [42CH Stepper Motor Driver Expansion Board].

![](images/assemble_base/motor_wire_1.jpg)
![](images/assemble_base/motor_wire_2.jpg)




![](images/assemble_base/fitting_dcsocket_1.jpg)
![](images/assemble_base/fitting_dcsocket_2.jpg)


Fit the [soldered DC socket](fromstep) into the circular side hole of the base, and secure it using a [DC socket nut]{qty:1} and a [DC socket washer]{qty:1}.

You have now successfully built the [assembled base]{output, Qty:1}. You will need this later.
