# Operation

## Plug in the device, it will perform a calibration step {pagestep}

![](images/operation/1.JPG)

## Rotate the slider and place the microscope slide in the slot {pagestep}

![](images/operation/2.JPG)

## Place the drop of blood onto the microscope slide {pagestep}

![](images/operation/3.JPG)
![](images/operation/4.JPG)

## Rotate the slider back into position {pagestep}

![](images/operation/5.JPG)

## Insert the spreader microscope slide into the slot of the slider {pagestep}

![](images/operation/6.JPG)

## Press the button to start the smear {pagestep}

![](images/operation/7.JPG)
![](images/operation/8.JPG)

## Remove the spreader slide {pagestep}

![](images/operation/9.JPG)

## Remove the microscope slide with the smear. {pagestep}

![](images/operation/10.JPG)
